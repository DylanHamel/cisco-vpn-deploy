# cisco-vpn-deploy

###### Dylan Hamle - <dylan.hamel@protonmail.com> - August 2019

Deploy your Cisco VPN IPSec site-to-site with Ansible. You have to define a YAML per client.
It's EASY :).



### Run

```shell
ansible-playbook -i hosts deploy_client.yml \
		--vault-password-file=.ansible_vault_key \
		--extra-vars="host_run=client_01"
```



### Validate client file

```shell
» ./client_file_check.py --file=host_vars/client_01.yml
[client_file_check - main] - File is OK :) !
------------------------------------------------------------
» ./client_file_check.py --dir=host_vars/                                 
[client_file_check - main] - File is OK :) !
```



### Client file example

If you see that some parameters are missing you can send me an email or open an issue :). I will add it.

```Yaml
vpn_client:
  - id: 1
    client: Clientun
    ike: v1
    # ike: v2
    peer_ip: 122.0.0.1

    # psk: toto-12-34++
    # keyring:											(IKEv2)
    #  local_key: key123						(IKEv2)	
    #  remote_key: key456						(IKEv2)

    phase_1:
      encr: aes256
      hash: sha256
      dh: 21
      lifetime: 86400
      auth: pre-share
      # mode:
      #   type: aggressive
      #   pass: aggr-password
      # prf: 24											(IKEv2)
      # local_auth: pre-share				(IKEv2)
      # remote_auth: pre-share			(IKEv2)
      
    phase_2:
      encr: aes256
      hash: sha256
      lifetime:
        based: seconds # OR "kilobytes" OR "days"
        value: 28800
      group: 21
      # mode: tunnel
      # pfs: 21
      # idle_time: 60								(IKEv2)	

    acl:
      type: extended
      # type: standard

    dnat:
      - in: 192.168.1.1
        out: 10.255.255.1
      - in: 192.168.1.10
        out: 10.255.255.10

    network:
      - action: permit
        protocol: tcp
        port_type: 22
        src:  
          type: host
          ip:
            - 10.255.254.1
        dst:
          type: host
          ip:
            - 10.255.255.10
      
      - action: permit
        protocol: icmp
        port_type: echo
        src:  
          type: host
          ip:
            - 10.255.254.1
        dst:
          type: host
          ip:
            - 10.255.255.1
            
      - action: permit
        protocol: ip
        src:
          type: network
          ip:
            - addr: 10.100.30.55
              wild: 0.0.0.255

        dst:
          type: host
          ip:
            - addr: 10.100.30.55
              wild: 0.0.0.255
```



### Troubleshooting

```shell
» ansible-playbook -i hosts show_commands.yml --extra-vars "ccmd='show crypto ipsec sa'"
» ansible-playbook -i hosts show_commands.yml --extra-vars "ccmd='show ip int brief'"
```



### PS

Don't try to crack ansible-vault files. Password is ``12toto34`` :D.