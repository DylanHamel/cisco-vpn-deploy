#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

"""
This file will validate your client_0X YAML file.

Actually check :

01- Phase_1:Lifetime
02- Phase_2:Lifetime in seconds/kilobytes/days
03- Phase_1:Encryption
04- Phase_2:Encryption
05- Phase_1:Hashing
06- Phase_2:Hashing
07- Ike:
08- Peer_ip:
09- Client:
10- Id:
11- Acl:Type
12- Network:Action
13- Network:Protocol
14- Network:Src:Type
15- Network:Dst:Type
16- Network:Src:Ip:Addr     (Network)
17- Network:Dst:Ip:Addr     (Network)
18- Network:Src:Ip:Addr     (Host)
19- Network:Dst:Ip:Addr     (Host)
20- Phase_2:Pfs
21- Phase_1:Auth
22- Check mandatory keys for IKEv1
23- Check mandatory keys for IKEv2
24- Phase_1:Hashing         (IKEv2)
25- Phase_1:Auth            (IKEv2)
26- Check phase_1 mandatory keys for IKEv1
27- Check phase_1 mandatory keys for IKEv2

There are no check for wildmask for the moment !!!

IKEV2
keyring:
      local_key: key123
      remote_key: key456

phase_1:
      local_auth: pre-share
      remote_auth: pre-share
      prf: 24

Modifier le Jinja2 pour supprimer

"""

__author__ = "Pictet Network Team"
__maintainer__ = "Network Team"
__version__ = "1.0"
__email__ = "netprod@pictet.com"
__status__ = "Production"
__copyright__ = "Copyright 2019"
__license__ = "MIT"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    import yaml
except ImportError as importError:
    print("Error import [check_file_client] yaml")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import ipaddress
except ImportError as importError:
    print("Error import [check_file_client] ipaddress")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from os import listdir
except ImportError as importError:
    print("Error import [check_file_client] listdir")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import click
except ImportError as importError:
    print("Error import [check_file_client] click")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import pprint
    PP = pprint.PrettyPrinter(indent=4)
except ImportError as importError:
    print("Error import [check_file_client] pprint")
    print(importError)
    exit(EXIT_FAILURE)
######################################################
#
# Constantes
#

VPN_IKEV1_MANDATORY_KEYS = ['id', 'client', 'ike', 'peer_ip',
                            'phase_1',  'phase_2',  'acl',  'network']

VPN_IKEV2_MANDATORY_KEYS = ['id', 'client', 'ike', 'peer_ip', 'keyring',
                            'phase_1',  'phase_2',  'acl',  'network']

IKEV1_PHASE_1_MANDATORY_KEYS = ['encr', 'hash', 'dh', 'lifetime', 'auth']
IKEV2_PHASE_1_MANDATORY_KEYS = ['encr', 'hash', 'dh', 'lifetime', 'local_auth', 'remote_auth']

PHASE_2_MANDATORY_KEYS = ['encr', 'hash']

# ----------------------------------------------------
CLIENT_ID = [1, 65535]

IKE_VERSION = ['v1', 'v2']

ENCRYPTION_PARAM = ['aes', 'aes128', 'aes192', 'aes256']

HASHING_PARAM = ['md5', 'sha', 'sha256', 'sha384', 'sha512']
HASHING_IKEV2_PARAM = ['md5', 'sha1', 'sha256', 'sha384', 'sha512']

DH_GROUP_PARAM = [1, 2, 5, 14, 15, 16, 19, 20, 21, 24]

PHASE_1_LIFETIME = [60, 86400]
PHASE_1_MODE = ['aggressive', 'main']
PHASE_1_AUTH = ['pre-share', 'rsa-encr', 'rsa-sig']
PHASE_1_IKEV2_AUTH = ['eap', 'ecdsa-sig', 'pre-share', 'rsa-sig']

PHASE_2_LIFETIME_SECONDS = [120, 2592000]
PHASE_2_LIFETIME_DAYS = [1, 30]
PHASE_2_LIFETIME_KILOBYTES = [2560, 4294967295]
PHASE_2_PFS = ['group1', 'group2', 'group5', 'group14', 'group15',
               'group16', 'group19', 'group20', 'group21', 'group24']
LIFETIME_BASED = ['seconds', 'kilobytes', 'days']

VPN_MODE = ['tunnel', 'transport']

ACL_TYPE = ['standard', 'extended']
ACL_ACTION = ['permit', 'deny']
ACL_PROTOCOL = ['ahp', 'eigrp', 'esp', 'gre', 'icmp', 'igmp', 'ip', 'ipinip',
                'nos', 'object-group', 'ospf', 'pcp', 'pim', 'sctp', 'tcp', 'udp']

NETWORK_SRC_DST_TYPE = ['host', 'network', 'any', 'object-group']
# ----------------------------------------------------
ENTRY_YAML = 'vpn_client'
CLIENT_YAML = 'client'
IKE_KEY_YAML = 'ike'
PEER_IP_YAML = 'peer_ip'
ID_YAML = 'id'

YAML_PHASE_1 = 'phase_1'
P1_ENCR = 'encr'
P1_HASH = 'hash'
P1_DH = 'dh'
P1_LIFETIME = 'lifetime'
P1_AUTH = 'auth'
P1_IKEV2_REMOTE_AUTH = 'remote_auth'
P1_IKEV2_LOCAL_AUTH = 'local_auth'
P1_MODE = 'mode'
P1_MODE_TYPE = 'type'
P1_MODE_PASSWD = 'pass'


YAML_PHASE_2 = 'phase_2'
P2_ENCR = 'encr'
P2_HASH = 'hash'
P2_DH = 'group'
P2_PFS = 'pfs'
P2_MODE = 'mode'
P2_LIFETIME = 'lifetime'
P2_LIFETIME_VALUE = 'value'
P2_LIFETIME_BASED = 'based'

ACL_YAML = 'acl'
ACL_TYPE_YAML = 'type'
NETWORK_YAML = 'network'
NETWORK_ACTION_YAML = 'action'
NETWORK_PROTOCOL_YAML = 'protocol'
NETWORK_SRC_YAML = 'src'
NETWORK_DST_YAML = 'dst'
NETWORK_SRC_DST_TYPE_YAML = 'type'
NETWORK_SRC_DST_IP_YAML = 'ip'
NETWORK_IP_ADDR_YAML = 'addr'
NETWORK_IP_WILD_YAML = 'addr'


######################################################
#
# Functions
#

def open_file(path):
    """
    This function  will open a yaml file and return is data

    Args:
        param1 (str): Path to the yaml file

    Returns:
        str: YAML content
    """

    with open(path, 'r') as yamlFile:
        try:
            data = yaml.load(yamlFile)
        except yaml.YAMLError as exc:
            print(exc)

    return data


def has_to_be_in(value, liste):
    return (value in liste)


def has_to_be_between(value, liste):
    return (not (value < liste[0] or value > liste[1]))


def check_mandatory_keys(dictionnary, mandatory_keys):
    # Check that there are all keys in YAML file
    lst_keys_missing = list()
    for mandatory_key in mandatory_keys:
        if mandatory_key not in dictionnary.keys():
            lst_keys_missing.append(mandatory_key)

    if len(lst_keys_missing) > 0:
        print(
            "[client_file_check - check_mandatory_keys] The following keys are missing {}.".format(lst_keys_missing))
        return False

    return True


def check_file(path_to_file):

    file_is_ok = True

    try:
        client_content = open_file(path_to_file)
        if 'vpn_client' in client_content.keys():

            for vpn_with_client in client_content[ENTRY_YAML]:
                 # Check ike value
                if not has_to_be_in(vpn_with_client[IKE_KEY_YAML], IKE_VERSION):
                    print("\n[client_file_check - check_file] - Error with ike: value in {}.".format(path_to_file))
                    print("[client_file_check - check_file] - This value ({}) has to be in {IKE_VERSION}.\n".format(vpn_with_client[IKE_KEY_YAML]))
                    file_is_ok = False

                # Check if mandatory keys are presents
                if vpn_with_client[IKE_KEY_YAML] == 'v1':
                    file_is_ok = check_mandatory_keys(vpn_with_client, VPN_IKEV1_MANDATORY_KEYS)
                    file_is_ok = check_mandatory_keys(vpn_with_client[YAML_PHASE_1], IKEV1_PHASE_1_MANDATORY_KEYS)
                elif vpn_with_client[IKE_KEY_YAML] == 'v2':
                    file_is_ok = check_mandatory_keys(vpn_with_client, VPN_IKEV2_MANDATORY_KEYS)
                    file_is_ok = check_mandatory_keys(vpn_with_client[YAML_PHASE_1], IKEV2_PHASE_1_MANDATORY_KEYS)

                check_mandatory_keys(vpn_with_client[YAML_PHASE_2], PHASE_2_MANDATORY_KEYS)

                # Check that peer_ip is an ip address
                try:
                    ipaddress.ip_address(u"{}".format(vpn_with_client[PEER_IP_YAML]))
                except ValueError as error:
                    print("\n[client_file_check - check_file] - Error with ip_peer: value in {}.".format(path_to_file))
                    print("[client_file_check - check_file] - This value ({}) is not a valide ip address.\n".format(vpn_with_client[PEER_IP_YAML]))
                    file_is_ok = False

                # Check client name
                if not str(vpn_with_client[CLIENT_YAML]).isalpha():
                    print("\n[client_file_check - check_file] - Error with client: value in {}.".format(path_to_file))
                    print("[client_file_check - check_file] - This value ({}) can contain only alpha.\n".format(vpn_with_client[CLIENT_YAML]))
                    file_is_ok = False

                # Check ID
                if not has_to_be_between(vpn_with_client[ID_YAML], CLIENT_ID):
                    print("\n[client_file_check - check_file] - Error with id: value in {path_to_file}.".format(path_to_file))
                    print("[client_file_check - check_file] - This value ({}) has to be between {}.\n".format(vpn_with_client[ID_YAML], CLIENT_ID))
                    file_is_ok = False

                # Check phase_1 authentification method
                if vpn_with_client[IKE_KEY_YAML] == 'v1':
                    if not has_to_be_in(vpn_with_client[YAML_PHASE_1][P1_AUTH], PHASE_1_AUTH):
                        print("\n[client_file_check - check_file] - Error with phase_1:auth value in {}.".format(path_to_file))
                        print("[client_file_check - check_file] - This value ({}) has to be in {}.\n".format(vpn_with_client[YAML_PHASE_1][P1_AUTH], PHASE_1_AUTH))
                        file_is_ok = False

                elif vpn_with_client[IKE_KEY_YAML] == 'v2':
                    if not has_to_be_in(vpn_with_client[YAML_PHASE_1][P1_IKEV2_REMOTE_AUTH], PHASE_1_IKEV2_AUTH):
                        print("\n[client_file_check - check_file] - Error with phase_1:remote_auth value in {}.".format(path_to_file))
                        print("[client_file_check - check_file] - This value ({}) has to be in {}.\n".format(vpn_with_client[YAML_PHASE_1][P1_IKEV2_REMOTE_AUTH], PHASE_1_IKEV2_AUTH))
                        file_is_ok = False

                    if not has_to_be_in(vpn_with_client[YAML_PHASE_1][P1_IKEV2_LOCAL_AUTH], PHASE_1_IKEV2_AUTH):
                        print("\n[client_file_check - check_file] - Error with phase_1:local_auth value in {}.".format(path_to_file))
                        print("[client_file_check - check_file] - This value ({}) has to be in {}.\n".format(vpn_with_client[YAML_PHASE_1][P1_IKEV2_LOCAL_AUTH], PHASE_1_IKEV2_AUTH))
                        file_is_ok = False

                # Check encryption param
                if not has_to_be_in(vpn_with_client[YAML_PHASE_1][P1_ENCR], ENCRYPTION_PARAM):
                    print("\n[client_file_check - check_file] - Error with phase_1: encr value in {}.".format(path_to_file))
                    print("[client_file_check - check_file] - This value has to be in {}.\n".format(ENCRYPTION_PARAM))
                    file_is_ok = False
                if not has_to_be_in(vpn_with_client[YAML_PHASE_2][P2_ENCR], ENCRYPTION_PARAM):
                    print("\n[client_file_check - check_file] - Error with phase_2: encr value in {}.".format(path_to_file))
                    print("[client_file_check - check_file] - This value has to be in {}.\n".format(ENCRYPTION_PARAM))
                    file_is_ok = False

                # Check hash param
                if vpn_with_client[IKE_KEY_YAML] == 'v1':
                    if not has_to_be_in(vpn_with_client[YAML_PHASE_1][P1_HASH], HASHING_PARAM):
                        print("\n[client_file_check - check_file] - Error with IKEv1 phase_1:hash value in {}.".format(path_to_file))
                        print("[client_file_check - check_file] - This value ({}) has to be in {}.\n".format(vpn_with_client[YAML_PHASE_1][P1_HASH], HASHING_PARAM))
                        file_is_ok = False
                elif vpn_with_client[IKE_KEY_YAML] == 'v2':
                    if not has_to_be_in(vpn_with_client[YAML_PHASE_1][P1_HASH], HASHING_IKEV2_PARAM):
                        print("\n[client_file_check - check_file] - Error with IKEv2 phase_1:hash value in {}.".format(path_to_file))
                        print("[client_file_check - check_file] - This value ({}) has to be in {}.\n".format(vpn_with_client[YAML_PHASE_1][P1_HASH], HASHING_IKEV2_PARAM))
                        file_is_ok = False

                if not has_to_be_in(vpn_with_client[YAML_PHASE_2][P2_HASH], HASHING_PARAM):
                    print("\n[client_file_check - check_file] - Error with phase_2: hash value in {}.".format(path_to_file))
                    print("[client_file_check - check_file] - This value has to be in {}.\n".format(HASHING_PARAM))
                    file_is_ok = False

                # Check DH group param
                if not has_to_be_in(vpn_with_client[YAML_PHASE_1][P1_DH], DH_GROUP_PARAM):
                    print("\n[client_file_check - check_file] - Error with phase_1: dh value in {}.".format(path_to_file))
                    print("[client_file_check - check_file] - This value has to be in {}.\n".format(DH_GROUP_PARAM))
                    file_is_ok = False
                if P2_DH in vpn_with_client[YAML_PHASE_2].keys():
                    if not has_to_be_in(vpn_with_client[YAML_PHASE_2][P2_DH], DH_GROUP_PARAM):
                        print("\n[client_file_check - check_file] - Error with phase_2: group value in {}.".format(path_to_file))
                        print("[client_file_check - check_file] - This value has to be in {}.\n".format(DH_GROUP_PARAM))
                        file_is_ok = False

                # Check phase_2 PFS group param
                if P2_PFS in vpn_with_client[YAML_PHASE_2].keys():
                    if not has_to_be_in(vpn_with_client[YAML_PHASE_2][P2_PFS], PHASE_2_PFS):
                        print("\n[client_file_check - check_file] - Error with phase_2:pfs value in {}.".format(path_to_file))
                        print("[client_file_check - check_file] - This value ({}) has to be in \n\t{}.\n".format(vpn_with_client[YAML_PHASE_2][P2_PFS], PHASE_2_PFS))
                        file_is_ok = False

                # Check lifetime value
                if not has_to_be_between(vpn_with_client[YAML_PHASE_1][P1_LIFETIME], PHASE_1_LIFETIME):
                    print("\n[client_file_check - check_file] - Error with phase_1: lifetime value in {}.".format(path_to_file))
                    print("[client_file_check - check_file] - This value has to be between {}.\n".format(PHASE_1_LIFETIME))
                    file_is_ok = False
                if P2_LIFETIME in vpn_with_client[YAML_PHASE_2].keys():
                    if not has_to_be_in(vpn_with_client[YAML_PHASE_2][P2_LIFETIME][P2_LIFETIME_BASED], LIFETIME_BASED):
                        print("\n[client_file_check - check_file] - Error with phase_2:lifetime:based: value in {}.".format(path_to_file))
                        print("[client_file_check - check_file] - This value has to be in {}.\n".format(LIFETIME_BASED))
                        file_is_ok = False
                    else:
                        if P2_LIFETIME_VALUE in vpn_with_client[YAML_PHASE_2][P2_LIFETIME].keys() and  \
                            P2_LIFETIME_BASED in vpn_with_client[YAML_PHASE_2][P2_LIFETIME].keys():
                            if vpn_with_client[YAML_PHASE_2][P2_LIFETIME][P2_LIFETIME_BASED] == 'seconds':
                                if not has_to_be_between(vpn_with_client[YAML_PHASE_2][P2_LIFETIME][P2_LIFETIME_VALUE], PHASE_2_LIFETIME_SECONDS):
                                    print("\n[client_file_check - check_file] - Error with phase_2: lifetime value in {}.".format(path_to_file))
                                    print("[client_file_check - check_file] - This value in seconds has to be between {}.\n".format(PHASE_2_LIFETIME_SECONDS))
                                    file_is_ok = False
                            elif vpn_with_client[YAML_PHASE_2][P2_LIFETIME][P2_LIFETIME_BASED] == 'kilobytes':
                                if not has_to_be_between(vpn_with_client[YAML_PHASE_2][P2_LIFETIME][P2_LIFETIME_VALUE], PHASE_2_LIFETIME_KILOBYTES):
                                    print("\n[client_file_check - check_file] - Error with phase_2: lifetime value in {}.".format(path_to_file))
                                    print("[client_file_check - check_file] - This value in kilobytes has to be between {}.\n".format(PHASE_2_LIFETIME_KILOBYTES))
                                    file_is_ok = False
                            elif vpn_with_client[YAML_PHASE_2][P2_LIFETIME][P2_LIFETIME_BASED] == 'days':
                                if not has_to_be_between(vpn_with_client[YAML_PHASE_2][P2_LIFETIME][P2_LIFETIME_VALUE], PHASE_2_LIFETIME_DAYS):
                                    print("\n[client_file_check - check_file] - Error with phase_2: lifetime value in {}.".format(path_to_file))
                                    print("[client_file_check - check_file] - This value in days has to be between {}.\n".format(PHASE_2_LIFETIME_DAYS))
                                    file_is_ok = False

                        else:
                            if not has_to_be_in(vpn_with_client[YAML_PHASE_2][P2_LIFETIME][P2_LIFETIME_VALUE], PHASE_2_LIFETIME_SECONDS):
                                print("\n[client_file_check - check_file] - Error with phase_2: lifetime value in {}.".format(path_to_file))
                                print("[client_file_check - check_file] - This value in seconds has to be between {}.\n".format(PHASE_2_LIFETIME_SECONDS))
                                file_is_ok = False

                # Check ACL type
                if not has_to_be_in(vpn_with_client[ACL_YAML][ACL_TYPE_YAML], ACL_TYPE):
                    print("\n[client_file_check - check_file] - Error with acl:type value in {}.".format(path_to_file))
                    print("[client_file_check - check_file] - This value has to be in {}.\n".format(ACL_TYPE))
                    file_is_ok = False

                # Check network (ACL) action
                for network_acl in vpn_with_client[NETWORK_YAML]:
                    if not has_to_be_in(network_acl[NETWORK_ACTION_YAML], ACL_ACTION):
                        print("\n[client_file_check - check_file] - Error with network:action value in {}.".format(path_to_file))
                        print("[client_file_check - check_file] - This value has to be in {}.\n".format(ACL_ACTION))
                        file_is_ok = False

                    if not has_to_be_in(network_acl[NETWORK_PROTOCOL_YAML], ACL_PROTOCOL):
                        print("\n[client_file_check - check_file] - Error with network:protocol :value in {}.".format(path_to_file))
                        print("[client_file_check - check_file] - This value has to be in \n\t {}.\n".format(ACL_PROTOCOL))
                        file_is_ok = False

                    if not has_to_be_in(network_acl[NETWORK_SRC_YAML][NETWORK_SRC_DST_TYPE_YAML], NETWORK_SRC_DST_TYPE):
                        print("\n[client_file_check - check_file] - Error with network:src:type value in {}.".format(path_to_file))
                        print("[client_file_check - check_file] - This value has to be in \n\t{}.\n".format(NETWORK_SRC_DST_TYPE))
                        file_is_ok = False

                    if not has_to_be_in(network_acl[NETWORK_DST_YAML][NETWORK_SRC_DST_TYPE_YAML], NETWORK_SRC_DST_TYPE):
                        print("\n[client_file_check - check_file] - Error with network:dst:type value in {}.".format(path_to_file))
                        print("[client_file_check - check_file] - This value has to be in \n\t{}.\n".format(NETWORK_SRC_DST_TYPE))
                        file_is_ok = False

                    if network_acl[NETWORK_SRC_YAML][NETWORK_SRC_DST_TYPE_YAML] == 'network':
                        lst_network_error = list()
                        for ip in network_acl[NETWORK_SRC_YAML][NETWORK_SRC_DST_IP_YAML]:
                            if NETWORK_IP_WILD_YAML not in ip.keys() or NETWORK_IP_ADDR_YAML not in ip.keys():
                                print("\n[client_file_check - check_file] - Error with network:src:ip:addr (network) value in {}.".format(path_to_file))
                                print("\n[client_file_check - check_file] - Some values are required. With network you have to specify addr: and wild:.")
                                file_is_ok = False
                            else:
                                try:
                                    ipaddress.ip_address(u"{}".format(ip[NETWORK_IP_ADDR_YAML]))
                                except ValueError as error:
                                    print("\n[client_file_check - check_file] - Error with network:src:ip:addr (network) value in {}.".format(path_to_file))
                                    print("[client_file_check - check_file] - This value is not a valide ip address ({}).\n".format(ip[NETWORK_IP_ADDR_YAML]))
                                    file_is_ok = False

                    elif network_acl[NETWORK_SRC_YAML][NETWORK_SRC_DST_TYPE_YAML] == 'host':
                        for ip in network_acl[NETWORK_SRC_YAML][NETWORK_SRC_DST_IP_YAML]:
                            try:
                                ipaddress.ip_address(u"{}".format(ip))
                            except ValueError as error:
                                print("\n[client_file_check - check_file] - Error with network:src:ip (host) value in {}.".format(path_to_file))
                                print("[client_file_check - check_file] - This value is not a valide ip address ({}).\n".format(ip))
                                file_is_ok = False


                    if network_acl[NETWORK_DST_YAML][NETWORK_SRC_DST_TYPE_YAML] == 'network':
                        for ip in [NETWORK_DST_YAML][NETWORK_SRC_DST_IP_YAML]:
                            if NETWORK_IP_WILD_YAML not in ip.keys() or NETWORK_IP_ADDR_YAML not in ip.keys():
                                print("\n[client_file_check - check_file] - Error with network:dst:ip:addr (network) value in {}.".format(path_to_file))
                                print("\n[client_file_check - check_file] - Some values are required. With network you have to specify addr: and wild:.")
                                file_is_ok = False
                            else:
                                try:
                                    ipaddress.ip_address(u"{}".format(ip[NETWORK_IP_ADDR_YAML]))
                                except ValueError as error:
                                    print("\n[client_file_check - check_file] - Error with network:dst:ip:addr (network) value in {}.".format(path_to_file))
                                    print("[client_file_check - check_file] - This value is not a valide ip address ({}).\n".format(ip))
                                    file_is_ok = False

                    elif network_acl[NETWORK_DST_YAML][NETWORK_SRC_DST_TYPE_YAML] == 'host':
                        for ip in network_acl[NETWORK_DST_YAML][NETWORK_SRC_DST_IP_YAML]:
                            try:
                                ipaddress.ip_address(u"{}".format(ip))
                            except ValueError as error:
				print(error)
                                print("\n[client_file_check - check_file] - Error with network:dst:ip (host) value in {}.".format(path_to_file))
                                print("[client_file_check - check_file] - This value is not a valide ip address ({}).\n".format(ip))
                                file_is_ok = False


    except AttributeError as error:
        print("[client_file_check - check_file] - Error with {}".format(path_to_file))
        print("[client_file_check - check_file] - {error}".format(error))
        file_is_ok = False

    return file_is_ok

######################################################
#
# MAIN Functions
#
@click.command()
@click.option('--file', default='#', help='Path to client YAML file.')
@click.option('--dir', default='#', help='Path directory that contains all client YAML files.')
def main(file, dir):

    if file == '#' and dir == '#':
        print("[client_file_check - main] Please specify client YAML file or folder path.")
        exit(EXIT_FAILURE)

    if file != '#' and dir != '#':
        print("[client_file_check - main] Please set only one of two arguments --file or --dir.")
        exit(EXIT_FAILURE)

    file_is_ok = bool
    if dir != '#':
        dirs = listdir(dir)
        for file in dirs:
            file_is_ok = check_file(str("{}/{}".format(dir, file)))


    if file != '#':
        file_is_ok = check_file(str("{}".format(file)))

    if file_is_ok:
        print("[client_file_check - main] - File is OK :) !")
        exit(EXIT_SUCCESS)
    else:
        print("[client_file_check - main] - File is NOK :( !")
        exit(EXIT_FAILURE)

# -----------------------------------------------------------------------------------------------------------------------------
#
#
if __name__ == "__main__":
    main()