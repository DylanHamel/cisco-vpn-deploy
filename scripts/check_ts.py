#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
This file will check if a transform-set already exist on a Cisco router.

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Production"
__copyright__ = "Copyright 2019"
__license__ = "MIT"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    from netmiko import ConnectHandler
except ImportError as importError:
    print("Error import [check_ts.py] netmiko")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import click
except ImportError as importError:
    print("Error import [check_ts.py] click")
    print(importError)
    exit(EXIT_FAILURE)

######################################################
#
# Constantes
#

IOS_SHOW_TS_CMD = "show crypto ipsec transform-set {}"
IOS_SHOW_TS_ERR = "Transform set {} not found"

######################################################
#
# Functions
#


######################################################
#
# MAIN Functions
#
@click.command()
@click.option('--ip', default='#', help='Cisco router IP address.')
@click.option('--usr', default="#", help='Cisco router username.')
@click.option('--pwd', default="#", help='Cisco router password.')
@click.option('--ssh', default=22, help='Cisco router ssh port.')
@click.option('--ts', default="#", help='Transform-set to check.')
def main(ip, usr, pwd, ssh, ts):

    cisco = {
       'device_type': 'cisco_ios',
       'host': ip,
       'username': usr,
       'password': pwd,
    }
    net_connect = ConnectHandler(**cisco)
    output = net_connect.send_command(f"show crypto ipsec transform-set {ts}")

    print(output)

    if IOS_SHOW_TS_ERR.format(ts) == output:
        print(f":(:(:(:(:(:(:(:(:(:(:(:(:(:(:(:(:(:(:(")
        print(f"TS ({ts}) DOESN'T EXIST !!!!!!")
        print(f":(:(:(:(:(:(:(:(:(:(:(:(:(:(:(:(:(:(:(")
        exit(EXIT_FAILURE)
    else:
        print(f":):):):):):):):):):):):):):):):):):):)")
        print(f"TS ({ts}) EXIST !!!!!")
        print(f":):):):):):):):):):):):):):):):):):):)")

        exit(EXIT_SUCCESS)

######################################################
#
#
if __name__ == "__main__":
    main()
