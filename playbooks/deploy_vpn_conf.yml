---
- hosts: all
  connection: local
  vars:
    to_push_dir: "./config/_to_push/"
    provider_creds:
      host: "{{ ansible_host }}"
      username: "{{ username }}"
      password: "{{ password }}"
  vars_files:
    - ./phase_1/param.yml
    - ./phase_2/param.yml
  tasks:


    - name: Generate ISAKMP from template
      run_once: True
      delegate_to: localhost
      template:
        src: ./templates/isakmp_template.j2
        dest: ./phase_1/policy.cfg
      register: generate_isakmp


    - name: Print phase_1/policy.cfg content
      run_once: Trues
      delegate_to: localhost
      debug:
        msg: "{{ lookup('file', './phase_1/policy.cfg')}}"
      when: generate_isakmp.changed


    - name: Deploy ISAKMP profile
      ios_config:
        provider: "{{ provider_creds }}"
        lines:
          - "{{ item }}"
      with_items: "{{ lookup('file', './phase_1/policy.cfg')}}"
      when: generate_isakmp.changed and inventory_hostname in groups['test-router']


    - name: Generate TRANSFORM-SET from template
      delegate_to: localhost
      run_once: True
      template:
        src: ./templates/transform-set_template.j2
        dest: ./phase_2/policy.cfg
      register: generate_ts


    - name: Print phase_2/policy.cfg content
      delegate_to: localhost
      run_once: True
      debug:
        msg: "{{ lookup('file', './phase_1/policy.cfg')}}"
      when: generate_ts.changed


    - name: Deploy TRANSFORM-SET profile
      ios_config:
        provider: "{{ provider_creds }}"
        lines:
          - "{{ item }}"
      with_items: "{{ lookup('file', './phase_2/policy.cfg')}}"
      when: generate_ts.changed and inventory_hostname in groups['test-router']

    
    - name: Check if client VPN YAML file is ok with ./client_file_check.py
      script: ./client_file_check.py --file=./host_vars/{{inventory_hostname}}.yml
      register: yaml_file_ok
      when: inventory_hostname in groups['clients']


    - name: Print ./client_file_check.py output
      debug: 
        var: yaml_file_ok.stdout_line
      when: inventory_hostname in groups['clients']


    - name: Generate client VPN config from template
      template:
        src: ./templates/vpn_template.j2
        dest: ./config/{{ inventory_hostname }}_vpn.cfg
      register: client_template
      when: inventory_hostname in groups['clients']


    - name: Create ./config/_to_push/ folder
      delegate_to: localhost
      run_once: true
      file:
        path: ./config/_to_push/
        state: directory


    - name: Check that ./config/_to_push/ exists
      delegate_to: localhost
      run_once: true
      stat: 
        path: "{{ to_push_dir }}"
      register: in_prod_exists


    - name: Copy client VPN config files if changes
      command: cp ./config/{{ inventory_hostname }}_vpn.cfg {{ to_push_dir }}{{ inventory_hostname }}_vpn.cfg
      when: client_template.changed and inventory_hostname in groups['clients']

    
    - name: Retrieve all vpn config files
      find:
        paths: "{{ to_push_dir }}"
        patterns: "*.cfg"
      register: files_matched
      when: inventory_hostname in groups['test-router'] or inventory_hostname in groups['clients']


    - name: Print client VPN config files
      run_once: True
      debug:
        msg: "{{ files_matched.files | map(attribute='path') | list }}"
      when: inventory_hostname in groups['test-router']


    - name: Print client VPN config files contents
      run_once: True
      debug:
        msg: "{{ lookup('file', item) }}"
      with_items: "{{ files_matched.files | map(attribute='path') | list }}"
      when: inventory_hostname in groups['test-router']


    - name: Deploy client VPN config
      ios_config:
        provider: "{{ provider_creds }}"
        lines:
          - "{{ lookup('file', item) }}"
      with_items: "{{ files_matched.files | map(attribute='path') | list }}"
      when: inventory_hostname in groups['test-router']

    - name: Remove client VPN config files from _to_push directory
      file:
        path: ./config/_to_push/{{ inventory_hostname }}_vpn.cfg
        state: absent
      when: client_template.changed and inventory_hostname in groups['clients']
